=== TEI Search ===

== Overview == 

The TEI Search module adds search functions to the TEI Content module. Its key feature is that it is aware of some TEI tags.

The TEI Search module is part of a package of modules called TEICHI framework. This name brings together the Text Encoding Initiative (TEI) and Computer-Human Interaction (CHI). This framework functions in a Drupal 7 environment and consists of three modules: TEI Search (described here, still in development), TEI Content (the base module: currently at http://drupal.org/sandbox/christof.s/1118172), and TEI Download (currently at http://drupal.org/sandbox/christof.s/1120822).

Possible use cases for the TEICHI framework are text edition projects in literary studies, history, or other text-based disciplines, provided they have a relatively straightforward editorial situation: only one given edition of a text is documented, a single-column presentation makes sense, and authorial and editorial annotation are important. The package of modules could also be of use in educational contexts, e.g. workshops on electronic textual editing.

== Features == 

Search for terms in TEI-encoded texts.
Sensitive to some TEI tags
Able to deal with some special characters.
Allows you to limit your search in ways to exclude editor's notes or to include only quotes
Allows to search the linear transcription (orig/sic/abbr) or the reading text (reg/corr/expan)

== Requirements and Compatibility == 

The prerequisites are those of the TEI Content module. In addition, the TEI Content module itself is of course required.

The module is tested with and works for Firefox (currently 3.6), Chrome (currently 8.0), and Internet Explorer (currently 8). The module is currently known to be compatible with Drupal�s Garland and Bartik themes.
Installation and activation

The installation follows the standard procedure for modules in Drupal 7. Please consult the relevant Drupal documentation (http://drupal.org/documentation/install/modules-themes).

Run a cron job after installing the module and after uploading new files or modifying existing files to your TEICHI project so that they will be indexed.


== CONTACT == 
Christof Sch�ch (christof.s) - http://drupal.org/user/1152238/
Coder: Dmitrij Funkner.
Project supervisors: Lutz Wegner and Sebastian Pape, Dept. for Electrical Engineering and Computer Science, University of Kassel, Germany.