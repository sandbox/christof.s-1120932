<?php
// $Id$
?>
<?php if ($teisearch_results) : ?>
  <h2><?php print t('Search results for: '.$keys);?></h2>
  <ul class="teisearch-results">
    <?php print $teisearch_results; ?>
  </ul>
<?php print $pageroffset; ?>
<?php else : ?>
  <h2><?php print t('Your search yielded no results');?></h2>
<?php endif; ?>